//
//  ViewController.swift
//  AttendanceList
//
//  Created by Christian Potdevin on 8/17/19.
//  Copyright © 2019 Christian Potdevin. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController {
    
    var cbCentralManager = CBCentralManager.init()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func runBluetooth(_ sender: UIButton) {
        if !cbCentralManager.isScanning {
            cbCentralManager.scanForPeripherals(withServices: [])
            sender.setTitle("Stop Bluetooth", for: UIControl.State.normal)
        } else {
            cbCentralManager.stopScan()
            sender.setTitle("Run Bluetooth", for: UIControl.State.normal)
        }
    }
}

